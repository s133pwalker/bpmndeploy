# Проект BPMNDeploy

Проект BPMNDeploy представляет собой инструмент для разворачивания BPMN схем в среде Camunda BPM.

## Описание

Этот проект включает в себя классы и инструменты для автоматизации развертывания BPMN схем на сервере Camunda BPM. Он также содержит тесты(TODO) для проверки правильности развертывания.

## Требования

- Java 8 или выше
- Maven
- Camunda BPM
- PostgreSQL

## Настройка PostgreSQL

```
sudo -u postgres psql
CREATE DATABASE mydatabase;
CREATE USER myuser WITH PASSWORD 'mypassword';
GRANT ALL PRIVILEGES ON DATABASE mydatabase TO myuser;
```

## Посмотреть информацию о процессах в таблице:
```
psql -U myuser -d mydatabase
SELECT * FROM act_re_deployment;
```
## Установка и запуск

1. Клонируйте репозиторий на вашу локальную машину:

    ```
    git clone git@gitlab.com:s133pwalker/bpmndeploy.git
    ```

2. Откройте проект в вашей IDE и запустите его. Я использую Intellij IDEA

## Использование

1. Подготовьте BPMN файл, который вы хотите развернуть.

2. Укажите путь к вашему файлу BPMN в коде или в настройках.

3. Запустите проект.

4. После успешного развертывания вы получите сообщение о завершении процесса.

## Тестирование(TODO)

Проект содержит набор тестов для проверки корректности развертывания BPMN схем. Вы можете запустить их с помощью JUnit.

## Вопросы и поддержка

Если у вас возникли вопросы или проблемы, пожалуйста, создайте Issue в этом репозитории, либо напишите автору в Telegram https://t.me/martynov_egor.
