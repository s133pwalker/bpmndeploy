package com.example.utils;

import java.io.InputStream;
import java.util.Objects;

/**
 * Утилита для загрузки BPMN файлов из ресурсов.
 */
public class BPMNLoader {

    private static final String DEFAULT_BPMN_FILE_PATH = "/bpmn/hello_world.bpmn";
    private String bpmnFilePath = "/bpmn/hello_world.bpmn";

    /**
     * Создает экземпляр загрузчика BPMN файлов с указанным путем.
     */
    public BPMNLoader() {
        this.bpmnFilePath = bpmnFilePath;
    }

    /**
     * Загружает BPMN файл из ресурсов.
     *
     * @return входной поток для загруженного BPMN файла
     * @throws NullPointerException если файл не найден
     */
    public InputStream loadBPMN() {
        InputStream inputStream = getClass().getResourceAsStream(bpmnFilePath);
        Objects.requireNonNull(inputStream, "BPMN file not found: " + bpmnFilePath);
        return inputStream;
    }
}
