package com.example;

import com.example.utils.BPMNLoader;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngineConfiguration;

import java.io.InputStream;

public class Main {

    public static void main(String[] args) {
        try {
            // Создание и конфигурация движка процессов Camunda
            ProcessEngine processEngine = createProcessEngine();

            // Загрузка BPMN схемы
            BPMNLoader bpmnLoader = new BPMNLoader();
            InputStream bpmnStream = bpmnLoader.loadBPMN();

            // Деплой BPMN схемы
            BPMNDeployer bpmnDeployer = new BPMNDeployer(processEngine);
            bpmnDeployer.deployBPMN(bpmnStream, "HelloWorldDeployment");

            // Закрытие ресурсов
            processEngine.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static ProcessEngine createProcessEngine() {
        // Параметры подключения к базе данных
        String jdbcUrl = "jdbc:postgresql://localhost:5432/mydatabase";
        String jdbcUsername = "myuser";
        String jdbcPassword = "mypassword";
        String jdbcDriver = "org.postgresql.Driver";

        return ProcessEngineConfiguration.createStandaloneProcessEngineConfiguration()
                .setJdbcUrl(jdbcUrl)
                .setJdbcUsername(jdbcUsername)
                .setJdbcPassword(jdbcPassword)
                .setJdbcDriver(jdbcDriver)
                .setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE)
                .buildProcessEngine();
    }
}