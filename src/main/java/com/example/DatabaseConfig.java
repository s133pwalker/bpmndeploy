package com.example;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Properties;

public class DatabaseConfig {

    private static final String JDBC_PROPERTIES_FILE = "/jdbc.properties";

    private String url;
    private String username;
    private String password;

    public DatabaseConfig() {
        loadProperties();
    }

    private void loadProperties() {
        try (InputStream input = getClass().getResourceAsStream(JDBC_PROPERTIES_FILE)) {
            if (input == null) {
                throw new IOException("JDBC properties file not found");
            }
            Properties prop = new Properties();
            prop.load(input);
            url = Objects.requireNonNull(prop.getProperty("jdbc.url"), "jdbc.url cannot be null");
            username = Objects.requireNonNull(prop.getProperty("jdbc.username"), "jdbc.username cannot be null");
            password = Objects.requireNonNull(prop.getProperty("jdbc.password"), "jdbc.password cannot be null");
        } catch (IOException ex) {
            System.err.println("Error loading JDBC properties: " + ex.getMessage());
        }
    }

    public Connection getConnection() throws SQLException {
        try {
            return DriverManager.getConnection(url, username, password);
        } catch (SQLException ex) {
            System.err.println("Error connecting to the database: " + ex.getMessage());
            throw ex;
        }
    }
}