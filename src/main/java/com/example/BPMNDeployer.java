package com.example;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.repository.Deployment;
import org.camunda.bpm.engine.repository.DeploymentBuilder;

import java.io.InputStream;

public class BPMNDeployer {

    private final ProcessEngine processEngine;

    public BPMNDeployer(ProcessEngine processEngine) {
        this.processEngine = processEngine;
    }

    public void deployBPMN(InputStream bpmnFile, String deploymentName) {
        try (bpmnFile) {
            RepositoryService repositoryService = processEngine.getRepositoryService();

            // Создание нового билдера для деплоя
            DeploymentBuilder deploymentBuilder = repositoryService.createDeployment()
                    .name(deploymentName)
                    .addInputStream("hello_world.bpmn", bpmnFile);

            // Деплой BPMN схемы
            Deployment deployment = deploymentBuilder.deploy();

            // Вывод информации о деплое
            System.out.println("BPMN схема успешно развернута. Deployment ID: " + deployment.getId());
        } catch (Exception e) {
            System.err.println("Ошибка при деплое BPMN схемы: " + e.getMessage());
            e.printStackTrace();
        }
    }
}